import { Component, ElementRef, ViewChild } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { interval } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { InteractivityChecker } from '@angular/cdk/a11y';

export interface List {
  name: string;
  value: string;
  rvalue?: string;
  arr: Array<string>;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'saturday-loto';
  lists: Array<List> = [];
  name = '';
  showValues = false;

  playAudio(): void {
    const audio = new Audio();
    audio.src = 'assets/win31.mp3';
    audio.load();
    audio.play();
  }

  createList(): void {
    if (this.name.length !== 0 && !this.listIsInArr(this.name)) {
      this.lists.push({ name: this.name, arr: [], value: '' });
      this.name = '';
    }
  }

  remove(listName: string): void {
    this.lists = this.lists.filter(el => el.name !== listName);
  }

  listIsInArr(name: string): boolean {
    if (this.lists.some(el => el.name === name)) {
      return true;
    }
    return false;
  }

  trackByFn(index: any, item: any): string {
    return item.id; // unique id corresponding to the item
  }

  random(min: number, max: number) {
    return Math.random() * (max - min) + min;
  }

  generateRandom(): void {
    this.showValues = true;
    const until = 30;
    let counter = 0;

    const generate = () => {
      for (const el of this.lists) {
        el.rvalue = el.arr[Math.floor(Math.random() * el.arr.length)];
      }
    };

    const interv = interval(100).subscribe((val) => {
      generate();
      if (val === 30) {
        interv.unsubscribe();
        const interv2 = interval(250).subscribe((val2) => {
          generate();
          if (val2 === 15) {
            interv2.unsubscribe();
            const interv3 = interval(800).subscribe(val3 => {
              if (val3 === 3) {
                interv3.unsubscribe();
                this.playAudio();
                this.confetti({
                  particleCount: 100,
                  spread: 70,
                  origin: { y: 0.6 }
                });
              }
            })
          }
        });
      }
    });

  }

  movieNight(): void {
    this.lists = [
      {
        name: 'Food',
        arr: [],
        value: ''
      },
      {
        name: 'Drink',
        arr: [],
        value: ''
      },
      {
        name: 'Snack',
        arr: [],
        value: ''
      },
      {
        name: 'Movie',
        arr: [],
        value: ''
      }
    ];
  }

  clear(): void {
    this.lists = [];
  }

  confetti(args: any): any {
    return window['confetti'].apply(this, arguments);
  }

  drop(event: CdkDragDrop<string[]>): void {
    moveItemInArray(this.lists, event.previousIndex, event.currentIndex);
  }
}
