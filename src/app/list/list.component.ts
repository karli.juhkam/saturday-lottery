import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Input() list: any = [];
  value: any;
  @Input() rvalue: any;
  @Input() showValues = false;
  @Output() remove: EventEmitter<string> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  addToList(value: string): void {
    this.list.arr.push(value);
    this.value = '';
  }

  removeSelf(): void {
    this.remove.emit(this.list.name);
  }

  removeFromList(value: string): void {
    this.list.arr = this.list.arr.filter((el: string) => el !== value);
  }

  isDisabled(name: string): boolean {
    const valueExists = this.list.arr.includes(name);
    if (valueExists) {
      return true;
    }
    return false;
  }
}
